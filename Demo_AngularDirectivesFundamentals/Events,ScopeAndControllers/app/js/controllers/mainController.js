app.controller('mainCtrl', function ($scope) {
    $scope.user1 = {
        name: 'Luke Skywalker'
        , address: {
            street: 'PO Box 123'
            , city: 'Secret Rabel Base'
            , planet: 'Yavin 4'
        }
        , friends: [
      ' Han'
      , 'Leia'
      , 'Chewbacca'


    ]
    }
    $scope.user2 = {
        name: 'Han Solo'
        , address: {
            street: 'PO Box 123'
            , city: 'Secret Rabel Base'
            , planet: 'Yavin 4'
        }
        , friends: [
      ' Han'
      , 'Leia'
      , 'Chevbaca'


    ]
    }
});
app.directive('removeFriend', function () {
    return {
        restrict: 'E'
        , scope: {
            notifyParent: '&method'
        }
        , templateUrl: 'removeFriend.html'
        , controller: function ($scope) {
            $scope.removing = false;
            $scope.startRemove = function () {
                $scope.removing = true;
            }
            $scope.cancelRemove = function () {
                $scope.removing = false;
            }
            $scope.confirmRemove = function () {
                $scope.notifyParent();
            }
        }
    }
});
app.directive('userInfoCard', function () {
    return {
        templateUrl: "userInfoCard.html"
        , restrict: "E"
        , scope: {
            user: '='
            , initalCollapsed: '@collapsed'
        }
        , controller: function ($scope) {
            // $scope.collapsed=false;
            $scope.collapsedState = ($scope.initalCollapsed === 'true');
            $scope.knightMe = function (user) {
                user.rank = "knight";
            }
            $scope.collapse = function () {
                $scope.collapsed = !$scope.collapsed;
            }
            $scope.removeFriend = function (friend) {
                var idx = $scope.user.friends.indexOf(friend);
                if (idx > -1) {
                    $scope.user.friends.splice(idx,1);
                }
            }
        }
    }
});
app.directive('address', function () {
    return {
        restrict: 'E'
        , scope: true
        , templateUrl: 'address.html'
        , controller: function ($scope) {
            $scope.collapsed = false;
            $scope.collapseAddress = function () {
                $scope.collapsed = true;
            }
            $scope.expandAddress = function () {
                $scope.collapsed = false;
            }
        }
    }
});