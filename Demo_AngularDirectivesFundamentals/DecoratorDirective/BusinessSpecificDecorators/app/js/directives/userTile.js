'use strict';

app.directive('userTile', function() {
  return {
    restrict: 'E',
    scope: {
      user: '='
    },
    templateUrl: 'templates/userTile.html',
    
  }
});