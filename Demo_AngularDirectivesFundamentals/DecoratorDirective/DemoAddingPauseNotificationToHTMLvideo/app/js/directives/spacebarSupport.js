'use strict';

app.directive('spacebarSupport',function(){
    
    return{
        restrict:'A',
        link:function(scope,el,attrs){
            $('body').on('keypress',function(evt){
                 var vidE1=el[0];
                
                if(evt.keyCode===32){
                   
                   if(vidE1.paused){
                       vidE1.play();
                   }else{
                       vidE1.pause();
                   }
                }
            })
        }
        
        
    }
});