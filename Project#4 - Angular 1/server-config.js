module.exports = {
    PORT: 5000,            //Server Port
    PUBLIC: '/client/',    //Public Folder
    HOMEFILE: 'index.html' //Default File to load
};