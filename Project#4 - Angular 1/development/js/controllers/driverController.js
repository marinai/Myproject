(function() {
    'use strict';

    var app = angular.module('F1App');

    app.controller('driverController', function(driversService, $stateParams) {

        var vm = this;
        vm.test = 'test';
        vm.id = $stateParams.id;
        driversService.getDriverDetails(vm.id)
            .then(function(response) {
                console.log('driverDetails', response);
                vm.driver = response.data.MRData.StandingsTable.StandingsLists[0].DriverStandings[0];
            })
            .catch(function(error) {
                console.log('desila se greska kod getDriverDetails poziva', error);
                throw error;
            });

        driversService.getDriverRaces(vm.id)
            .then(function(response) {

                vm.races = response.data.MRData.RaceTable.Races;
            })
            .catch(function(error) {
                console.log('desila se greska kod getDriverRaces poziva', error);
                throw error;
            });
    });
}());
