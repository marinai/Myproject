(function() {
    'use strict';

    var app = angular.module('F1App');
    app.controller('teamController', function(driversService,$stateParams) {

        var vm = this;
        vm.test = 'test';
        vm.id = $stateParams.id;
        
        
        driversService.getTeamDetails(vm.id)
            .then(function(response) {
                console.log('teamDetails', response);
                vm.team = response.data.MRData.StandingsTable.StandingsLists[0].ConstructorStandings[0];
            })
            .catch(function(error) {
                console.log('desila se greska kod getTeamDetails poziva', error);
                throw error;
            });

        driversService.getTeamResults(vm.id)
            .then(function(response) {
                vm.results = response.data.MRData.RaceTable;
            })
            .catch(function(response) {
                throw response;
            });
        
        
//        Gojkovo resenje za getTotal
        
         vm.getTotal = function(index) {
            var total = 0;
            for (var i = 0; i < vm.results.Races[index].Results.length; i++) {
                var product = vm.results.Races[index].Results[i];
                total += Number(product.points);
            }
            return total;
        };
            
           });
}());



//continue here!!!!!!!!!!!!!!!!!!!!!!!!!!!!! 