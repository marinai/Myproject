(function() {
    'use strict';

    var app = angular.module('F1App');
    app.controller('teamsController', function(driversService) {

        var vm = this;
         vm.nameFilter = null;

        vm.test = 'test';
         driversService.getTeams()
            .then(function(response) {
                console.log(`response`);
                console.log(response);
                vm.teamsList = response.data.MRData.StandingsTable.StandingsLists[0].ConstructorStandings;
            })
            .catch(function(error) {
                console.log('desila se greska kod getConstructors poziva', error);
                throw error;
            });
        
        
//        search 
         vm.searchFilter = function(team) {
            var keyword = new RegExp(vm.nameFilter,'i');
            return !vm.nameFilter || keyword.test(team.Constructor.name) || keyword.test(team.Constructor.nationality);
        };
        
           });
}());