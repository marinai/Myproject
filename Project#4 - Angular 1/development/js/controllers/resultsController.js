(function() {
    'use strict';

    var app = angular.module('F1App');
    app.controller('resultsController', function(driversService,$stateParams) {

        var vm = this;
        vm.nameFilter = null;

        vm.test = 'test';
        
        vm.id = $stateParams.id;
        console.log('ID: ' + vm.id);
        
        
        
        driversService.qualifyingResults(vm.id)
            .then(function(response) {
                vm.qualify = response.data.MRData.RaceTable.Races[0];
            })
            .catch(function(response) {
                throw response;
            });
		
		 driversService.getResults(vm.id)
            .then(function(response) {
                vm.results = response.data.MRData.RaceTable.Races[0];
            })
            .catch(function(response) {
                throw response;
            });
		
		
		
        
            });
}());
