(function () {
	'use strict';
	var app = angular.module('F1App');
	app.controller('racesController', function (driversService) {
		var vm = this;
		vm.nameFilter = null;
		vm.test = 'test';
		driversService.getRaces().then(function (response) {
			//			console.log(`getRacesResponse`);
			console.log(response);
			vm.racesList = response.data.MRData.RaceTable;
		}).catch(function (response) {
			throw response;
		});
		//        search 
		vm.searchFilter = function (race) {
			var keyword = new RegExp(vm.nameFilter, 'i');
			return !vm.nameFilter || keyword.test(race.raceName) || keyword.test(race.Circuit.circuitName);
		};
	});
}());