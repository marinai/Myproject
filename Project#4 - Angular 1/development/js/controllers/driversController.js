/* jshint maxlen: 200*/
(function() {
    'use strict';

    var app = angular.module('F1App');
    app.controller('driversController', function(driversService) {

        var vm = this;
        vm.nameFilter = null;

        vm.test = 'test';
        driversService.getDrivers()
            .then(function(response) {
                console.log(`response`);
                console.log(response);
                vm.driversList = response.data.MRData.StandingsTable.StandingsLists[0].DriverStandings;
            })
            .catch(function(error) {
                console.log('desila se greska kod getDrivers poziva', error);
                throw error;
            });

        vm.searchFilter = function(driver) {
            console.log('Driver:', driver);
            var keyword = new RegExp(vm.nameFilter, 'i');
            console.log(keyword);
            // jscs:disable
            return !vm.nameFilter || keyword.test(driver.Driver.givenName) || keyword.test(driver.Driver.familyName);
        };
    });
}());