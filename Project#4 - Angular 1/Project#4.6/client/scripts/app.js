'use strict';

var app = angular.module('F1App', ['ui.router']);

app.config(function($stateProvider, $urlRouterProvider) {

    $urlRouterProvider.otherwise('/');

    $stateProvider
        .state('drivers', {
        url: '/',
            templateUrl: '/templates/drivers.html',
            controller: 'driversController',
            controllerAs: 'drivers'
        })

    .state('driver', {
        url: '/drivers/{id}',
        templateUrl: '/templates/driver.html',
        controller: 'driverController',
        controllerAs: 'driver'
    })
        .state('teams', {
        url: '/teams',
        templateUrl: '/templates/teams.html',
        controller: 'teamsController',
        controllerAs: 'teams'
    })
    .state('team', {
            url: '/teams/{id}',
            templateUrl: '/templates/team.html',
            controller: 'teamController',
            controllerAs: 'team'
        })
    .state('results', {
            url: '/results/{id}',
            templateUrl: '/templates/results.html',
            controller: 'resultsController',
            controllerAs: 'results'
        })
		.state('races', {
            url: '/races',
            templateUrl: '/templates/races.html',
            controller: 'racesController',
            controllerAs: 'races'
        });

});